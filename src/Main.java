import java.net.MalformedURLException;
import java.net.URL;

/**
 * Main for testing WordCounter.
 * @author Voraton Lertrattanapaisal
 *
 */
public class Main {
	/**
	 * Main for testing WordCounter.
	 * @param args is do nothing.
	 * @throws MalformedURLException for avoid MalformedURLException.
	 */
	public static void main(String[] args) throws MalformedURLException {
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL( DICT_URL );
		WordCounter counter = new WordCounter();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		System.out.println("Reading words from "+DICT_URL);
		System.out.printf("Counted %,d syllables in %,d words\n",syllables,wordcount);
		System.out.printf("Elapsed time: %.3f",counter.getElapsed());
	}

}
