import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
/**
 * WordCounter is using for counting word and syllables.
 * @author Voraton Lertrattanapaisal
 *
 */
public class WordCounter {
	private State state;
	private StopWatch timer;
	private int syllables;
	private int counter = 0;
	/**
	 * Constructor of WordCounter that initialize the StopWatch.
	 */
	public WordCounter(){
		timer = new StopWatch();
	}
	/**
	 * Consonant is state that isn't vowels and special character.
	 */
	State Consonant = new State(){
		@Override
		public void handleChar(char c) {
			if (isVowel(c)) setState(Vowel);
			else if (isLetter(c)) setState(Consonant);
			else if (isSpecial(c)) setState(Special);
			else setState(NonWord);
		}
		@Override
		public void enterState() {
			syllables++;
		}
		@Override
		public void setState(State newstate){
			if (newstate == Vowel) enterState();
			if (newstate == NonWord) newstate.enterState();
			state = newstate;
		}
	};
	/**
	 * Vowel is state that is vowel.
	 */
	State Vowel = new State(){
		@Override
		public void handleChar(char c) {
			if (isVowel(c)) setState(Vowel);
			else if (isLetter(c)) setState(Consonant);
			else if (isSpecial(c)) setState(Special);
			else setState(NonWord);
		}
		@Override
		public void enterState() {
			syllables++;
		}
		@Override
		public void setState(State newstate){
			if (newstate == NonWord) newstate.enterState();
			state = newstate;
		}
	};
	/**
	 * Special is state that is special character.
	 */
	State Special = new State(){
		@Override
		public void handleChar(char c) {
			if (isVowel(c)) setState(Vowel);
			else if (isLetter(c)) setState(Consonant);
			else if (isSpecial(c)) setState(Special);
			else setState(NonWord);
		}
		@Override
		public void enterState() {
			syllables++;
		}
		@Override
		public void setState(State newstate){
			if (newstate == Vowel) enterState();
			if (newstate == NonWord) newstate.enterState();
			state = newstate;
		}
	};
	/**
	 * NonWord is state that is not the word.
	 */
	State NonWord = new State(){
		@Override
		public void handleChar(char c) {
			setState(NonWord);
		}
		@Override
		public void enterState() {
			syllables = 0;
		}
		@Override
		public void setState(State newstate){
			enterState();
		}
	};
	/**
	 * E_FIRST is state to check last e.
	 */
	State E_FIRST = new State(){
		@Override
		public void handleChar(char c) {
			setState(E_FIRST);
		}
		@Override
		public void enterState() {
			//do nothing
		}
		@Override
		public void setState(State newstate){
			state = newstate;
		}
	};
	/**
	 * isLetter use for checking if c is letter.
	 * @param c is character that want to be checked.
	 * @return true if c is character.
	 */
	private boolean isLetter(char c){
		if (Character.isLetter(c)) return true;
		return false;
	}
	/**
	 * isVowel use for checking if c is vowel.
	 * @param c is character that want to be checked.
	 * @return true if c is vowel.
	 */
	private boolean isVowel(char c){
		if ("AEIOUYaeiouy".indexOf(c) >=0) return true;
		return false;
	}
	/**
	 * isSpecial use for checking if c is special character.
	 * @param c is character that want to be checked.
	 * @return true if c is special character.
	 */
	private boolean isSpecial(char c){
		if ("\' -".indexOf(c) >=0) return true;
		return false;
	}
	/**
	 * For Counting the Syllables from the word.
	 * @param word is word for counting.
	 * @return number of syllables of word.
	 */
	public int countSyllables(String word){
		syllables = 0;
		state = Consonant;
		for (int i = 0 ;i<word.length();i++){
			char c = word.charAt(i);
			if ((i==0||i==word.length()-1)&& !isLetter(c)) state.setState(NonWord);
			if (i==word.length()-1&&"Ee".indexOf(c)>=0&&syllables!=0) state.setState(E_FIRST);
			if (state == NonWord){ 
				break;
			}
			state.handleChar(c);			
		}
		int count = syllables;
		syllables = 0;
		return count;
	}
	/**
	* Count all words read from an input stream an return the total count.
	* @param instream is the input stream to read from.
	* @return number of words in the InputStream. -1 if the stream
	* cannot be read.
	*/
	public int countWords(InputStream instream) {
		timer.start();
		BufferedReader reader = new BufferedReader( new InputStreamReader(instream) );
		int count = 0;
		try {
			while( true ){
				String line = reader.readLine();
				if (line==null) break;
				counter+=countSyllables(line);
				count++;
			}
			timer.stop();
			return count;
		}
		catch (IOException io){
			timer.stop();
			return -1;
		}
	}
	/**
	 * Count all the word from url and return total count.
	 * @param url is url that want to count.
	 * @return total count of word from url.
	 */
	public int countWords(URL url) {
		try {
			return countWords(url.openStream());
		}
		catch(IOException io){
			return -1;
		}
	}
	/**
	 * Get the elapsed time of counting in seconds.
	 * @return he elapsed time of counting.
	 */
	public double getElapsed(){
		return timer.getElapsed();
	}
	/**
	 * To get total number of syllable.
	 * @return total number of syllable.
	 */
	public int  getSyllableCount( ) {
		return counter;
	}
}
